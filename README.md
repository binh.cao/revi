# Revi

The utility to remove Vietnamese sign.
Ex: `CAO Quảng Bình` -> `CAO Quang Binh`.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'revi'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install revi

## Usage

```ruby
class MyAwesomeClass
  include Revi::VietnameseSignProcessor

  def my_awesome_function
    vietnamese_string = 'CAO Quảng Bình'
    unsigned = replace_vietnamese_sign(vietnamese_string)
    # unsigned => 'CAO Quang Binh'
  end
end
```

## Contributing

1. Fork [https://gitlab.com/binh.cao/revi](https://gitlab.com/binh.cao/revi)
2. Create your feature branch (`git checkout -b my-awesome-feature`)
3. Commit your changes (`git commit -m 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new pull request with a description of your changes

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

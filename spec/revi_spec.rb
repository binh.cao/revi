require 'spec_helper'
describe Revi do
  include Revi::VietnameseSignProcessor

  it 'has a version number' do
    expect(Revi::VERSION).not_to be nil
  end

  it 'Can replace Vietnamese char' do
    signed_chars  = 'àáảãạăắẳẵặâấầẫậèéẻẽẹêềếểễệìíỉĩịòóỏõọôồốổỗộơờớởỡợùúủũụưừứửữựỳýỷỹỵ
                     ÀÁẢÃẠĂẮẲẴẶÂẤẦẪẬÈÉẺẼẸÊỀẾỂỄỆÌÍỈĨỊÒÓỎÕỌÔỒỐỔỖỘƠỜỚỞỠỢÙÚỦŨỤƯỪỨỬỮỰỲÝỶỸỴđĐ'
    un_sign_chars = 'aaaaaaaaaaaaaaaeeeeeeeeeeeiiiiiooooooooooooooooouuuuuuuuuuuyyyyy
                     AAAAAAAAAAAAAAAEEEEEEEEEEEIIIIIOOOOOOOOOOOOOOOOOUUUUUUUUUUUYYYYYdD'
    processed_string = replace_vietnamese_sign(signed_chars)
    expect(processed_string).to eq un_sign_chars
  end
end

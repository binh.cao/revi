require_relative 'char_map'

module Revi::VietnameseSignProcessor
  include Revi::CharMap

  def replace_vietnamese_sign(source)
    replaced_string = ''
    source.split('').each do |char|
      replaced_string += REPLACEABLE_CHAR.key?(char) ? REPLACEABLE_CHAR[char] : char
    end
    replaced_string
  end
end

# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'revi/version'

Gem::Specification.new do |spec|
  spec.name          = 'revi'
  spec.version       = Revi::VERSION
  spec.authors       = ['CAO Quang Binh']
  spec.email         = ['caoquangbinh@gmail.com']

  spec.summary       = 'Remove Vietnamese sign'
  spec.description   = 'Remove Vietnamese sign'
  spec.homepage      = 'https://gitlab.com/binh.cao/revi'
  spec.license       = 'MIT'

  spec.files = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = 'bin'
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.13'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'

  spec.add_dependency 'thor'
end
